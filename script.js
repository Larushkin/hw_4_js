'use strict'

// Теория 

//1. Функции нужны для того, чтоб упращать код, чтоб повторять одно и то же действие в разных частях программы.
//
//2. Аргументы для функции являются обрабатываемыми переменными.
//   Аргументы нужны, если результат обработки зависит от входных данных.
//
//3. Оператор return – возвращает указанное значение – результат выполнения функции и завершает работу функции.


let a = prompt('Введите первое число');
let b = prompt('Введите второе число');
let c = prompt('Введите действие');
let num1 = checkNum(+a);
let num2 = checkNum(+b);
let action = checkAction(c);

function checkNum(num) {
    if (isNaN(+num) || +num == null) {
        while (isNaN(+num) || +num == null) {
            num = prompt('Введите число!', num);
        }
    }
    return(num);
}

function checkAction(action) {

    if (action !== '*' && action !== '/' && action !== '+' && action !== '-') {
        while (action !== '*' && action !== '/' && action !== '+' && action !== '-') {
            action = prompt('Введите действие', action);
        }
    }
    return(action);
}

function calculate(num1, num2, action) {
    let result;
    switch (action) {
        case '*':
            result = num1 * num2;
            break;
        case '/':
            result = num1 / num2;
            break;
        case '+':
            result = num1 + num2;
            break;
        case '-':
            result = num1 - num2;
            break;
    }
    return(result);
}
console.log(`Твои параметры: первое число = ${num1}, второе число = ${num2}, действие = ${action}`);
console.log(`Твой результат: ${calculate(num1, num2, action)}`);

